function Greeting(){
    var hour = new Date().getHours();
    var welcomeTypes = ["Good morning!", "Good afternoon!", "Good evening!"];
    var welcomeText = "";
    
    if (hour < 12) welcomeText = welcomeTypes[0];
    else if (hour < 18) welcomeText = welcomeTypes[1];
    else welcomeText = welcomeTypes[2];
    
    document.getElementById("titleGreeting").innerHTML = welcomeText;
}

var mybutton = document.getElementById("bbtBtn");
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

const observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    console.log(entry)
    if(entry.isIntersecting){
      entry.target.classList.add('show');}
    //else{
    //   entry.target.classList.remove('show');
    // }
  });
});

const hiddenelements = document.querySelectorAll('.hidden');
hiddenelements.forEach((el) => observer.observe(el));